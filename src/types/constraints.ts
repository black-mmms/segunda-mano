interface IsSameOptions {
  field: string;
}

export type { IsSameOptions };
