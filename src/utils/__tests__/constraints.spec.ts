import { expect } from '@jest/globals';
import {
  isEmail,
  isPassword,
  isSame,
  isRequired,
} from '@app/utils/constraints';

describe('Constraints', () => {
  it('email constraint', () => {
    expect(isEmail('email')).toBe('Ingrese una dirección de correo válida');
    expect(isEmail('email@email.email')).toBe('');
  });

  it('password constraint', () => {
    expect(isPassword('password')).toBe(
      'Tu contraseña debe contener 8 caracteres, por lo menos 1 letra y 1 numero'
    );
    expect(isPassword('Passw0rd')).toBe('');
  });

  it('same constraint', () => {
    expect(isSame('Passw0rd', { other: 'Password' }, { field: 'other' })).toBe(
      'Los campos no coinciden'
    );
    expect(isSame('Passw0rd', { other: 'Passw0rd' }, { field: 'other' })).toBe(
      ''
    );
  });

  it('required constraint', () => {
    expect(isRequired('')).toBe('Completa este campo');
    expect(isRequired('required')).toBe('');
  });
});
