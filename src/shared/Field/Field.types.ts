type Patterns = { [key in React.HTMLInputTypeAttribute]: string };

interface ContainerProps extends React.HTMLAttributes<HTMLDivElement> {
  variant?: 'error' | 'success';
}
interface FieldProps {
  helptext?: string;
  showPassword?: boolean;
  leftElement?: React.ReactNode;
  rightElement?: React.ReactNode;
  inputProps?: React.InputHTMLAttributes<HTMLInputElement>;
  variant?: 'error' | 'success';
}

export type { FieldProps, ContainerProps, Patterns };
