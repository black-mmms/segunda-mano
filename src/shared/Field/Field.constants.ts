import type { Patterns } from './Field.types';

const PATTERNS: Patterns = {
  email: '[a-z0-9._%+-]+@[a-z0-9.-]+.[a-zA-Z0-9-.]+',
  password: '(?=.*[0-9])(?=.*[A-Z])([a-zA-Z0-9!@#$%]+){8,}',
};

export { PATTERNS };
