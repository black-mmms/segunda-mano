import type { ModalProps } from './Modal.types';
import { useRef, useEffect, useState, useCallback } from 'react';
import { createPortal } from 'react-dom';
import * as Styled from './Modal.styles';

function ModalComponent({
  children,
  footer,
  title,
  onClosed,
  ...props
}: ModalProps): React.ReactElement {
  const container = useRef() as React.MutableRefObject<HTMLDivElement>;
  const modal = useRef() as React.MutableRefObject<HTMLDivElement>;

  const onClickDocument = useCallback(
    (event: MouseEvent) => {
      const target = event.target as Node;
      const isContentEvent = container.current.contains(target);
      const isModalEvent = modal.current.contains(target);

      if (!isContentEvent && isModalEvent) {
        onClosed?.();
      }
    },
    [container, modal, onClosed]
  );

  useEffect(() => {
    document?.addEventListener('click', onClickDocument);

    return () => {
      document?.removeEventListener('click', onClickDocument);
    };
  }, [container, onClickDocument]);

  return (
    <Styled.Modal {...props} ref={modal}>
      <Styled.Container ref={container}>
        <Styled.Closed onClick={onClosed} role="modal-closed" />
        <Styled.Content>
          {title && <Styled.Title forwardedAs="h2">{title}</Styled.Title>}
          {children}
        </Styled.Content>
        {footer && <Styled.Footer>{footer}</Styled.Footer>}
      </Styled.Container>
    </Styled.Modal>
  );
}

function Modal({
  open,
  ...props
}: ModalProps): React.ReactPortal | React.ReactElement {
  const [mounted, setMounted] = useState(false);

  useEffect(() => {
    setMounted(true);
  }, []);

  if (mounted && open) {
    return createPortal(
      <ModalComponent {...props} open={open} />,
      document.body
    );
  }

  return <></>;
}

export { Modal };
