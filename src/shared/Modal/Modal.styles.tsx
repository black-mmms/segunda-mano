import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faXmark } from '@fortawesome/free-solid-svg-icons';
import { Typography } from '@app/shared/Typography';

const Modal = styled.div`
  width: 100%;
  height: 100%;
  position: fixed;
  top: 0px;
  left: 0px;
  background-color: rgba(0, 0, 0, 0.6);
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

const Container = styled.div`
  width: calc(100% - 20px);
  max-width: 500px;
  background-color: #ffffff;
  max-height: calc(100% - 20px);
  overflow: auto;
  cursor: auto;
  position: relative;
`;
const Content = styled.div`
  width: 100%;
  padding: 40px;
  padding-bottom: 20px;
`;

const Closed = styled(FontAwesomeIcon).attrs(() => ({
  icon: faXmark,
}))`
  position: absolute;
  right: 0px;
  top: 0px;
  margin: 4px;
  cursor: pointer;
  width: 14px;
  height: auto;
  path {
    fill: #666666;
  }
`;

const Footer = styled.div`
  height: 48px;
  display: flex;
  background-color: #18b9b4;
  width: 100%;
  justify-content: center;
  align-items: center;
`;

const Title = styled(Typography)`
  text-align: center;
  font-size: 36px;
  font-weight: bold;
  margin-bottom: 40px;
`;

export { Modal, Container, Content, Closed, Footer, Title };
