interface ModalProps extends React.PropsWithChildren {
  onClosed?: () => void;
  footer?: React.ReactNode;
  title?: string;
  open: boolean;
}

export type { ModalProps };
