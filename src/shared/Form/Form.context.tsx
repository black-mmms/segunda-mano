import type {
  FormProps,
  ValidatorContext,
  FormContext as FormContextType,
} from './Form.types';
import React, { createContext, useEffect, useRef, useState } from 'react';
import { FormValidator } from './Form.utils';
import * as Styled from './Form.styles';

const FormContext = createContext<FormContextType>({});

function FormProvider({
  children,
  validations = {},
  ...props
}: FormProps): React.ReactElement {
  const [validatorContext, setValidatorContext] = useState<ValidatorContext>(
    {}
  );
  const [isSubmitting, setIsSubmitting] = useState<boolean>(false);
  const formValidator = useRef() as React.MutableRefObject<
    InstanceType<typeof FormValidator>
  >;
  const form = useRef() as React.MutableRefObject<HTMLFormElement>;

  useEffect(() => {
    if (form.current && validations) {
      const instance = new FormValidator(form.current, validations);
      formValidator.current = instance;
      setValidatorContext(instance?.context);
    }
  }, [form, validations]);

  const onChange = (event: React.ChangeEvent<HTMLFormElement>): void => {
    const context = formValidator.current.context;
    setValidatorContext(context);
    props?.onChange?.(event, context);
  };

  const onSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    const context = formValidator.current.context;
    setValidatorContext(context);
    setIsSubmitting(true);
    props?.onSubmit?.(event, context);
  };

  return (
    <FormContext.Provider value={{ ...validatorContext, isSubmitting }}>
      <Styled.Form
        ref={form}
        {...props}
        onSubmit={onSubmit}
        onChange={onChange}
      >
        {children}
      </Styled.Form>
    </FormContext.Provider>
  );
}

export { FormContext, FormProvider };
