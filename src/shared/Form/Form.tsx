import type { FormProps } from './Form.types';
import { FormProvider } from './Form.context';

function Form({ children, ...props }: FormProps): React.ReactElement {
  return <FormProvider {...props}>{children}</FormProvider>;
}

export { Form };
