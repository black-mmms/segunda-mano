import { expect } from '@jest/globals';
import { screen } from '@testing-library/dom';
import { FieldValidator, FormValidator } from '@app/shared/Form/Form.utils';

describe('Field Validator', () => {
  it('field validator email', () => {
    const fieldValidator = new FieldValidator().isEmail();
    expect(fieldValidator.validator('test', { email: 'test' }).error).toBe(
      'Ingrese una dirección de correo válida'
    );
    expect(
      fieldValidator.validator('email@email.email', {
        email: 'email@email.email',
      }).error
    ).toBe('');
  });

  it('field validator password', () => {
    const fieldValidator = new FieldValidator().isPassword();
    expect(fieldValidator.validator('test', { password: 'test' }).error).toBe(
      'Tu contraseña debe contener 8 caracteres, por lo menos 1 letra y 1 numero'
    );
    expect(
      fieldValidator.validator('Passw0rd', { password: 'Passw0rd' }).error
    ).toBe('');
  });

  it('field validator same', () => {
    const fieldValidator = new FieldValidator().isSame({ field: 'other' });
    expect(
      fieldValidator.validator('test', { field: 'test', other: 'testother' })
        .error
    ).toBe('Los campos no coinciden');
    expect(
      fieldValidator.validator('test', { field: 'test', other: 'test' }).error
    ).toBe('');
  });

  it('field validator required', () => {
    const fieldValidator = new FieldValidator().isRequired();
    expect(fieldValidator.validator('', { field: '' }).error).toBe(
      'Completa este campo'
    );
    expect(fieldValidator.validator('test', { field: 'test' }).error).toBe('');
  });

  it('field multiple validator', () => {
    const fieldValidator = new FieldValidator().isRequired().isEmail();
    expect(fieldValidator.validator('', { field: '' }).error).toBe(
      'Completa este campo'
    );
    expect(fieldValidator.validator('test', { field: 'test' }).error).toBe(
      'Ingrese una dirección de correo válida'
    );
    expect(
      fieldValidator.validator('email@email.email', {
        field: 'email@email.email',
      }).error
    ).toBe('');
  });
});

describe('Field Validator', () => {
  it('form invalid validator', () => {
    document.body.innerHTML = `
      <form data-testid="form">
        <input type='email' name='email' data-testid="form-email" />
      </form>
    `;
    const form = screen.queryByTestId('form') as HTMLFormElement;
    const field = screen.queryByTestId('form-email') as HTMLInputElement;
    const values = { email: '' };
    const fieldValidator = new FieldValidator()
      .isEmail()
      .validator('', values, field);
    const fields = {
      email: {
        value: '',
        field: field,
        error: 'Ingrese una dirección de correo válida',
        validator: fieldValidator,
      },
    };
    const isValid = false;

    const validations = {
      email: new FieldValidator().isEmail(),
    };
    const formValidator = new FormValidator(form, validations);

    expect(formValidator.entries).toStrictEqual([['email', '']]);
    expect(formValidator.formData).toStrictEqual(new FormData(form));
    expect(formValidator.values).toStrictEqual(values);
    expect(formValidator.fields).toStrictEqual(fields);
    expect(formValidator.isValid).toStrictEqual([fields, isValid]);
    expect(formValidator.context).toStrictEqual({
      values,
      form,
      fields,
      isValid,
    });
  });

  it('form valid validator', () => {
    document.body.innerHTML = `
      <form data-testid="form">
        <input type='email' name='email' value='email@email.email' data-testid="form-email" />
      </form>
    `;
    const form = screen.queryByTestId('form') as HTMLFormElement;
    const field = screen.queryByTestId('form-email') as HTMLInputElement;
    const values = { email: 'email@email.email' };
    const fieldValidator = new FieldValidator()
      .isEmail()
      .validator('email@email.email', values, field);
    const fields = {
      email: {
        value: 'email@email.email',
        field: field,
        error: '',
        validator: fieldValidator,
      },
    };
    const isValid = true;

    const validations = {
      email: new FieldValidator().isEmail(),
    };
    const formValidator = new FormValidator(form, validations);

    expect(formValidator.entries).toStrictEqual([
      ['email', 'email@email.email'],
    ]);
    expect(formValidator.formData).toStrictEqual(new FormData(form));
    expect(formValidator.values).toStrictEqual(values);
    expect(formValidator.fields).toStrictEqual(fields);
    expect(formValidator.isValid).toStrictEqual([fields, isValid]);
    expect(formValidator.context).toStrictEqual({
      values,
      form,
      fields,
      isValid,
    });
  });
});
