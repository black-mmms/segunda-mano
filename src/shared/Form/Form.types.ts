import { FieldValidator } from './Form.utils';

type Constraint = (value: string, values: { [key: string]: string }) => string;
type Fields = { [key: string]: Field };
type Errors = { [key: string]: string };
type FormValues = { [key: string]: string };
type Validations = { [key: string]: InstanceType<typeof FieldValidator> };

interface Field {
  error: string;
  value: string;
  field: HTMLInputElement;
  validator?: InstanceType<typeof FieldValidator>;
}
interface ValidatorContext {
  isValid?: boolean;
  values?: FormValues;
  fields?: Fields;
  form?: HTMLFormElement;
}
interface FormContext extends ValidatorContext {
  isSubmitting?: boolean;
}
interface FormProps
  extends Omit<
    React.FormHTMLAttributes<HTMLFormElement>,
    'onSubmit' | 'onChange'
  > {
  validations?: Validations;
  onChange?: (
    event: React.ChangeEvent<HTMLFormElement>,
    FormContext: FormContext
  ) => void;
  onSubmit?: (
    event: React.FormEvent<HTMLFormElement>,
    FormContext: FormContext
  ) => void;
}

export type {
  Fields,
  Field,
  Errors,
  FormValues,
  Validations,
  ValidatorContext,
  FormContext,
  Constraint,
  FormProps,
};
