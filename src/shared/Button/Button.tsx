import type { ButtonProps } from './Button.types';
import * as Styled from './Button.styles';

function Button(props: ButtonProps): React.ReactElement {
  return <Styled.Button {...props} />;
}

export { Button };
