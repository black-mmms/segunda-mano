import type { SignInModalProps } from './SignInModal.types';
import { Form } from './components/Form';
import { getValidations } from './SignInModal.utils';
import * as TR from './SignInModal.translations';
import * as Styled from './SignInModal.styles';

function SignInModal({
  onSignUp,
  onSubmit,
  ...props
}: SignInModalProps): React.ReactElement {
  const validations = getValidations();
  const footer = (
    <Styled.Footer>
      <Styled.FooterText>
        {TR.NOT_HAVE}{' '}
        <span onClick={onSignUp} data-test="signin-create-new">
          {TR.CREATE_NEW}
        </span>
      </Styled.FooterText>
    </Styled.Footer>
  );

  return (
    <Styled.Modal
      {...props}
      data-test="signin-modal"
      footer={footer}
      title={TR.LOGIN}
    >
      <Styled.Form
        autoComplete="off"
        validations={validations}
        onSubmit={onSubmit}
      >
        <Form />
      </Styled.Form>
    </Styled.Modal>
  );
}

export { SignInModal };
