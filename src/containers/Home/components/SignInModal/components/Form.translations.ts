import { i18next } from '@app/translations';

const HIDE = i18next.t('signInModal.hide', 'Hide');
const SHOW = i18next.t('signInModal.show', 'Show');
const ENTER = i18next.t('signInModal.enter', 'Enter');

const EMAIL_ERROR = i18next.t(
  'errors.email',
  'Please enter a valid email address'
);
const PASSWORD_ERROR = i18next.t(
  'errors.password',
  'Your password must contain 8 characters, at least 1 letter and 1 number'
);

const PLACEHOLDER_EMAIL = i18next.t('signInModal.placeholderEmail', 'Email');
const PLACEHOLDER_PASSWORD = i18next.t(
  'signInModal.placeholderPassword',
  'Password'
);

export {
  HIDE,
  SHOW,
  ENTER,
  EMAIL_ERROR,
  PASSWORD_ERROR,
  PLACEHOLDER_EMAIL,
  PLACEHOLDER_PASSWORD,
};
