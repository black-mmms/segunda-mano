import { useContext, useState } from 'react';
import { FormContext } from '@app/shared/Form/Form.context';
import * as TR from './Form.translations';
import * as Styled from './Form.styles';

function Form(): React.ReactElement {
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const { isValid } = useContext(FormContext);

  const onToggleShow = () => {
    setShowPassword((prev) => !prev);
  };

  const rightElement = (
    <Styled.Show data-test="toggle-password" onClick={onToggleShow}>
      {showPassword ? TR.SHOW : TR.HIDE}
    </Styled.Show>
  );

  return (
    <Styled.Content>
      <Styled.Field
        data-test="signin-email"
        inputProps={{
          placeholder: TR.PLACEHOLDER_EMAIL,
          autoComplete: 'off',
          name: 'email',
          type: 'email',
          required: true,
          title: TR.EMAIL_ERROR,
        }}
      />
      <Styled.Field
        data-test="signin-password"
        rightElement={rightElement}
        showPassword={showPassword}
        inputProps={{
          placeholder: TR.PLACEHOLDER_PASSWORD,
          autoComplete: 'off',
          name: 'password',
          type: 'password',
          required: true,
          title: TR.PASSWORD_ERROR,
        }}
      />
      <Styled.Button data-test="signin-submit" disabled={!isValid} full>
        {TR.ENTER}
      </Styled.Button>
    </Styled.Content>
  );
}

export { Form };
