import type { ModalProps } from '@app/shared/Modal/Modal.types';
import type { FormContext } from '@app/shared/Form/Form.types';

interface SignInModalProps extends ModalProps {
  onSignUp: () => void;
  onSubmit: (
    event: React.FormEvent<HTMLFormElement>,
    context: FormContext
  ) => void;
}

export type { SignInModalProps };
