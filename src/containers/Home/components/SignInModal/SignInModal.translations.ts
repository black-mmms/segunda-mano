import { i18next } from '@app/translations';

const NOT_HAVE = i18next.t(
  'signInModal.notHave',
  'You do not have an account?'
);
const CREATE_NEW = i18next.t('signInModal.createNew', 'Create a new');
const LOGIN = i18next.t('signInModal.logIn', 'Log in');

export { NOT_HAVE, CREATE_NEW, LOGIN };
