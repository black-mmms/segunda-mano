import { i18next } from '@app/translations';

const NOT_HAVE = i18next.t(
  'signUpModal.haveAccount',
  'You do have an account?'
);
const LOGIN = i18next.t('signUpModal.loginIn', 'Log in');
const CREATE_NEW = i18next.t('signUpModal.createAccount', 'Create a account');

export { NOT_HAVE, CREATE_NEW, LOGIN };
