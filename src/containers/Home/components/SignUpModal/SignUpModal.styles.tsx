import styled from 'styled-components';
import { Typography } from '@app/shared/Typography';
import { Modal as ModalShared } from '@app/shared/Modal';
import { Form as FormShared } from '@app/shared/Form';

const Modal = styled(ModalShared)``;

const Form = styled(FormShared)``;

const Footer = styled.div``;

const FooterText = styled(Typography)`
  color: #ffffff;
  font-size: 16px;
  font-weight: 400;
  span {
    text-decoration: underline;
    cursor: pointer;
  }
`;

export { Modal, Form, Footer, FooterText };
