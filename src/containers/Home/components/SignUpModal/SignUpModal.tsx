import type { SignUpModalProps } from './SignUpModal.types';
import { Form } from './components/Form';
import { getValidations } from './SignUpModal.utils';
import * as TR from './SignUpModal.translations';
import * as Styled from './SignUpModal.styles';

function SignUpModal({
  onSignIn,
  onSubmit,
  ...props
}: SignUpModalProps): React.ReactElement {
  const validations = getValidations();
  const footer = (
    <Styled.Footer>
      <Styled.FooterText>
        {TR.NOT_HAVE}{' '}
        <span onClick={onSignIn} data-test="signup-login">
          {TR.LOGIN}
        </span>
      </Styled.FooterText>
    </Styled.Footer>
  );

  return (
    <Styled.Modal
      {...props}
      data-test="signup-modal"
      footer={footer}
      title={TR.CREATE_NEW}
    >
      <Styled.Form
        autoComplete="off"
        validations={validations}
        onSubmit={onSubmit}
      >
        <Form />
      </Styled.Form>
    </Styled.Modal>
  );
}

export { SignUpModal };
