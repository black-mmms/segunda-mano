import type { ModalProps } from '@app/shared/Modal/Modal.types';
import type { FormContext } from '@app/shared/Form/Form.types';

interface SignUpModalProps extends ModalProps {
  onSignIn: () => void;
  onSubmit: (
    event: React.FormEvent<HTMLFormElement>,
    context: FormContext
  ) => void;
}

export type { SignUpModalProps };
