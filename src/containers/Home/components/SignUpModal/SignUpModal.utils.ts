import type { Validations } from '@app/shared/Form/Form.types';
import { FieldValidator } from '@app/shared/Form/Form.utils';

function getValidations(): Validations {
  return {
    email: new FieldValidator().isRequired().isEmail(),
    password: new FieldValidator()
      .isRequired()
      .isPassword()
      .isSame({ field: 'repeatPassword' }),
    repeatPassword: new FieldValidator()
      .isRequired()
      .isPassword()
      .isSame({ field: 'password' }),
  };
}

export { getValidations };
