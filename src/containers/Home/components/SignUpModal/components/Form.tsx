import { useContext } from 'react';
import { FormContext } from '@app/shared/Form/Form.context';
import * as TR from './Form.translations';
import * as Styled from './Form.styles';

function Form(): React.ReactElement {
  const { isValid } = useContext(FormContext);

  return (
    <Styled.Content>
      <Styled.Field
        data-test="signup-email"
        inputProps={{
          placeholder: TR.PLACEHOLDER_EMAIL,
          autoComplete: 'off',
          name: 'email',
          type: 'email',
          required: true,
          title: TR.EMAIL_ERROR,
        }}
      />
      <Styled.Field
        data-test="signup-password"
        inputProps={{
          placeholder: TR.PLACEHOLDER_PASSWORD,
          autoComplete: 'off',
          name: 'password',
          type: 'password',
          required: true,
          title: TR.PASSWORD_ERROR,
        }}
      />
      <Styled.Field
        data-test="signup-repeat-password"
        inputProps={{
          placeholder: TR.PLACEHOLDER_REPEAT_PASSWORD,
          autoComplete: 'off',
          name: 'repeatPassword',
          type: 'password',
          required: true,
          title: TR.PASSWORD_ERROR,
        }}
      />
      <Styled.Button data-test="signup-submit" disabled={!isValid} full>
        {TR.ENTER}
      </Styled.Button>
    </Styled.Content>
  );
}

export { Form };
