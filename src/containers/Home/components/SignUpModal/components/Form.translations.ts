import { i18next } from '@app/translations';

const ENTER = i18next.t('signUpModal.enter', 'Create an account');

const EMAIL_ERROR = i18next.t(
  'errors.email',
  'Please enter a valid email address'
);
const PASSWORD_ERROR = i18next.t(
  'errors.repeatPassword',
  'Your password must contain 8 characters, at least 1 letter, 1 number and match both passwords'
);

const PLACEHOLDER_EMAIL = i18next.t('signUpModal.placeholderEmail', 'Email');
const PLACEHOLDER_PASSWORD = i18next.t(
  'signUpModal.placeholderPassword',
  'Password'
);
const PLACEHOLDER_REPEAT_PASSWORD = i18next.t(
  'signUpModal.placeholderRepeatPassword',
  'Repeat password'
);

export {
  ENTER,
  EMAIL_ERROR,
  PASSWORD_ERROR,
  PLACEHOLDER_EMAIL,
  PLACEHOLDER_PASSWORD,
  PLACEHOLDER_REPEAT_PASSWORD,
};
