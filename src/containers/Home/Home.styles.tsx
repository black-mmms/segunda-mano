import { Button as ButtonShared } from '@app/shared/Button';
import styled from 'styled-components';

const Container = styled.div`
  min-width: 100vw;
  min-height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Button = styled(ButtonShared)``;

export { Container, Button };
