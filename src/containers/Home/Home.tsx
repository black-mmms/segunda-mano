import type { FormContext } from '@app/shared/Form/Form.types';
import { useState } from 'react';
import { SignInModal } from './components/SignInModal';
import { SignUpModal } from './components/SignUpModal';
import * as Styled from './Home.styles';

function Home() {
  const [openSignin, setOpenSignin] = useState(false);
  const [openSignup, setOpenSignup] = useState(false);

  const onSigninClick = () => {
    setOpenSignin(true);
    setOpenSignup(false);
  };

  const onSignUpClick = () => {
    setOpenSignin(false);
    setOpenSignup(true);
  };

  const onSignUpSubmit = (
    event: React.FormEvent<HTMLFormElement>,
    { isValid, values }: FormContext
  ) => {
    event.preventDefault();

    if (isValid) {
      console.warn('values: ', values);
      onSigninClick();
    }
  };
  const onSignInSubmit = (
    event: React.FormEvent<HTMLFormElement>,
    { isValid, values }: FormContext
  ) => {
    event.preventDefault();

    if (isValid) {
      console.warn('values: ', values);
      setOpenSignin(false);
    }
  };

  return (
    <Styled.Container>
      <Styled.Button data-test="button-signin" onClick={onSigninClick}>
        Signin
      </Styled.Button>
      <SignUpModal
        open={openSignup}
        onSignIn={onSigninClick}
        onClosed={() => setOpenSignup(false)}
        onSubmit={onSignUpSubmit}
      />
      <SignInModal
        open={openSignin}
        onSignUp={onSignUpClick}
        onClosed={() => setOpenSignin(false)}
        onSubmit={onSignInSubmit}
      />
    </Styled.Container>
  );
}

export { Home };
