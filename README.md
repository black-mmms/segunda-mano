## Segunda Mano Challenge
[Live Challenge](https://segunda-mano.haiku.studio/)

### Requirements

- **node** (recommended latest version)
- **npm** (recommended latest version)
- **yarn** (optional) (recommended latest version)
- **npm install** (install project packages)

### Run Project

To run the development environment
```bash
npm run dev
```
*Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.*

### Testing
```bash
npm run test -> Run cypress and jest tests
npm run cypress:run -> Run cypress tests, need to start server manually
npm run e2e -> Run cypress tests and run the server automatically
npm run jest:run -> Run jest tests
```