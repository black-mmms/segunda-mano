describe('SignIn Modal', () => {
  before(() => {
    cy.visit('/');
  });

  it('Show Modal SignIn', () => {
    cy.get('[data-test=button-signin]').click();
    cy.get('[data-test=signin-modal]').should('be.visible');
  });

  it('Modal SignIn toggle password Validate', () => {
    cy.get('[data-test=toggle-password]').click();
    cy.get('[data-test=signin-password] [role=field]')
      .invoke('attr', 'type')
      .should('eq', 'text');
    cy.get('[data-test=toggle-password]').click();
    cy.get('[data-test=signin-password] [role=field]')
      .invoke('attr', 'type')
      .should('eq', 'password');
  });

  it('Modal SignIn Fields Empty Validate', () => {
    cy.get('[data-test=signin-email] [role=field]').clear();
    cy.get('[data-test=signin-password] [role=field]').clear();

    cy.get('[data-test=signin-submit]').should('be.disabled');
  });

  it('Modal SignIn Fields Blur Errors', () => {
    cy.get('[data-test=signin-email] [role=field]').focus().blur();
    cy.get('[data-test=signin-email] [role=helptext]').should('be.visible');

    cy.get('[data-test=signin-password] [role=field]').focus().blur();
    cy.get('[data-test=signin-password] [role=helptext]').should('be.visible');

    cy.get('[data-test=signin-submit]').should('be.disabled');
  });

  it('Modal SignIn Invalid Fields Errors', () => {
    cy.get('[data-test=signin-email] [role=field]').type('email').blur();
    cy.get('[data-test=signin-email] [role=helptext]').should('be.visible');

    cy.get('[data-test=signin-password] [role=field]').type('password').blur();
    cy.get('[data-test=signin-password] [role=helptext]').should('be.visible');

    cy.get('[data-test=signin-submit]').should('be.disabled');
  });

  it('Modal SignIn Valid Fields', () => {
    cy.get('[data-test=signin-email] [role=field]')
      .clear()
      .type('email@email.com')
      .blur();
    cy.get('[data-test=signin-email] [role=helptext]').should('not.exist');

    cy.get('[data-test=signin-password] [role=field]')
      .clear()
      .type('Casa#1991')
      .blur();
    cy.get('[data-test=signin-password] [role=helptext]').should('not.exist');

    cy.get('[data-test=signin-submit]').should('be.enabled');
  });

  it('Modal SignIn Submit Validate', () => {
    cy.get('[data-test=signin-email] [role=field]')
      .clear()
      .type('email@email.com');
    cy.get('[data-test=signin-password] [role=field]')
      .clear()
      .type('Casa#1991');

    cy.get('[data-test=signin-submit]').click();
    cy.get('[data-test=signin-modal]').should('not.exist');
  });

  it('Modal SignIn Closed Validate', () => {
    cy.get('[data-test=button-signin]').click();

    cy.get('[data-test=signin-modal] [role=modal-closed]').click();
    cy.get('[data-test=signin-modal]').should('not.exist');

    cy.get('[data-test=button-signin]').click();
    cy.get('[data-test=signin-modal]').should('be.visible');

    cy.get('body').click(0, 0);
    cy.get('[data-test=signin-modal]').should('not.exist');
  });
});
