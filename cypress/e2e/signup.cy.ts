describe('SignUp Modal', () => {
  before(() => {
    cy.visit('/');
  });

  it('Show Modal SignUp', () => {
    cy.get('[data-test=button-signin]').click();
    cy.get('[data-test=signin-modal]').should('be.visible');
    cy.get('[data-test=signin-create-new]').click();
    cy.get('[data-test=signup-modal]').should('be.visible');
  });

  it('Modal SignUp Fields Empty Validate', () => {
    cy.get('[data-test=signup-email] [role=field]').clear();
    cy.get('[data-test=signup-password] [role=field]').clear();
    cy.get('[data-test=signup-repeat-password] [role=field]').clear();

    cy.get('[data-test=signup-submit]').should('be.disabled');
  });

  it('Modal SignUp Fields Blur Errors', () => {
    cy.get('[data-test=signup-email] [role=field]').focus().blur();
    cy.get('[data-test=signup-email] [role=helptext]').should('be.visible');

    cy.get('[data-test=signup-password] [role=field]').focus().blur();
    cy.get('[data-test=signup-password] [role=helptext]').should('be.visible');

    cy.get('[data-test=signup-repeat-password] [role=field]').focus().blur();
    cy.get('[data-test=signup-repeat-password] [role=helptext]').should(
      'be.visible'
    );

    cy.get('[data-test=signup-submit]').should('be.disabled');
  });

  it('Modal SignUp Invalid Fields Errors', () => {
    cy.get('[data-test=signup-email] [role=field]').type('email').blur();
    cy.get('[data-test=signup-email] [role=helptext]').should('be.visible');

    cy.get('[data-test=signup-password] [role=field]').type('password').blur();
    cy.get('[data-test=signup-password] [role=helptext]').should('be.visible');

    cy.get('[data-test=signup-repeat-password] [role=field]')
      .type('repeat password')
      .blur();
    cy.get('[data-test=signup-repeat-password] [role=helptext]').should(
      'be.visible'
    );

    cy.get('[data-test=signup-submit]').should('be.disabled');
  });

  it('Modal SignUp Passwords Fields Errors', () => {
    cy.get('[data-test=signup-email] [role=field]')
      .clear()
      .type('email@email.com')
      .blur();
    cy.get('[data-test=signup-password] [role=field]')
      .clear()
      .type('Casa#1991')
      .blur();
    cy.get('[data-test=signup-repeat-password] [role=field]')
      .clear()
      .type('Casa#1991')
      .blur();

    cy.get('[data-test=signup-email] [role=helptext]').should('not.exist');
    cy.get('[data-test=signup-password] [role=helptext]').should('not.exist');
    cy.get('[data-test=signup-repeat-password] [role=helptext]').should(
      'not.exist'
    );

    cy.get('[data-test=signup-submit]').should('be.enabled');

    cy.get('[data-test=signup-repeat-password] [role=field]')
      .clear()
      .type('Casa#1992')
      .blur();
    cy.get('[data-test=signup-password] [role=helptext]').should('be.visible');
    cy.get('[data-test=signup-repeat-password] [role=helptext]').should(
      'be.visible'
    );

    cy.get('[data-test=signup-submit]').should('be.disabled');
  });

  it('Modal SignUp Login Click Validate', () => {
    cy.get('[data-test=signup-login]').click();

    cy.get('[data-test=signup-modal]').should('not.exist');
    cy.get('[data-test=signin-modal]').should('be.visible');

    cy.get('[data-test=signin-create-new]').click();
    cy.get('[data-test=signup-modal]').should('be.visible');
  });

  it('Modal SignIn Submit Validate', () => {
    cy.get('[data-test=signup-email] [role=field]')
      .clear()
      .type('email@email.com')
      .blur();
    cy.get('[data-test=signup-password] [role=field]')
      .clear()
      .type('Casa#1991')
      .blur();
    cy.get('[data-test=signup-repeat-password] [role=field]')
      .clear()
      .type('Casa#1991')
      .blur();

    cy.get('[data-test=signup-submit]').click();

    cy.get('[data-test=signup-modal]').should('not.exist');
    cy.get('[data-test=signin-modal]').should('be.visible');
  });

  it('Modal SignUp Closed Validate', () => {
    cy.get('[data-test=signin-create-new]').click();
    cy.get('[data-test=signup-modal] [role=modal-closed]').click();
    cy.get('[data-test=signup-modal]').should('not.exist');
    cy.get('[data-test=signin-modal]').should('not.exist');

    cy.get('[data-test=button-signin]').click();
    cy.get('[data-test=signin-modal]').should('be.visible');
    cy.get('[data-test=signin-create-new]').click();
    cy.get('[data-test=signup-modal]').should('be.visible');

    cy.get('body').click(0, 0);
    cy.get('[data-test=signup-modal]').should('not.exist');
    cy.get('[data-test=signin-modal]').should('not.exist');
  });
});
