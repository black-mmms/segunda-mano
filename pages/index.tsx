import { Home } from '@app/containers/Home';

function HomePage(): JSX.Element {
  return <Home />;
}

export default HomePage;
